
package assignment2;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;

/*
* @author Emilie Popp
*
* I create the Master Problem in Benders Decomposition. 
* It is clear that the complicating variable in the Unit Commitment Problem 
* is u(g,t), since it is the binary variable taking value 0 or 1. 
* If I were to remove the u's from the problem, it would just be a linear 
* problem i.e. much easier to solve. 
*
* When you consider the structure of the problem you see that c(g,t) 
* only appear in the objective function and the constraint 1.b. Furthermore
* u is the only variable in constraint 1.c and 1.d. Because of this structure 
* where u and c is the only variables in the first three constraints I let 
* c and u stay in the objective function in the master problem and I keep 
* constraint 1.b, 1.c, and 1.d in the master problem. 
* When I do so, the u's choosen in the master problem will be better
* candidates to solve the subproblems. So even though c is a continues and non-
* complicating variable I decide to keep it in the objective of the 
* Master Problem.
*/ 


public class masterProblem {
        
    private final IloCplex model;
    private final IloIntVar u[][]; 
    private final IloNumVar c[][];
    private final IloNumVar phi;
    private final assignment2Problem ucp;
    
    public masterProblem(assignment2Problem ucp) throws IloException {
        this.ucp = ucp;
        
        // I create an IloCplex object to contain the model
        this.model = new IloCplex();
        
        /* 
        * The decision variables u and c in the Master Problem is created.
        * Since u is binary is has to be an IloIntVar, where each position is 
        * a boolVar. 
        */
        
        this.u = new IloIntVar[ucp.getgPowerGenerators()][ucp.gettTimeperiods()];
        for(int g = 1; g<= ucp.getgPowerGenerators(); g++){
            for(int t = 1; t <= ucp.gettTimeperiods(); t++){
                u[g-1][t-1] = model.boolVar() ;
            } 
        }
        
        /*
        * c is a non negative continuous variable so it is a IloNumVar where 
        * each position can take values from 0 to infinity. 
        */
        this.c = new IloNumVar[ucp.getgPowerGenerators()][ucp.gettTimeperiods()];
        for(int g = 1; g<= ucp.getgPowerGenerators(); g++){
            for(int t = 1; t <= ucp.gettTimeperiods(); t++){
                c[g-1][t-1] = model.numVar(0, Double.POSITIVE_INFINITY);
            }
        }
        
        /*
        * I create phi which go into the objective function of 
        * the master problem. 
        */
        this.phi = model.numVar(0, Double.POSITIVE_INFINITY,"phi");   
    
    /* 
    * I create the objective function of the Master Problem containing u and c.
    * First I create an empty linear numerical expression 
    */
    IloLinearNumExpr obj = model.linearNumExpr();
        
    // Then I add terms to the objective function
    
    //I add the first double sum in the original problem
    for(int g = 1; g<= ucp.getgPowerGenerators(); g++){
            for(int t = 1; t <= ucp.gettTimeperiods() ; t++){
                obj.addTerm(1, c[g-1][t-1]);
                obj.addTerm(ucp.getCommitmentCost()[g-1], u[g-1][t-1]);
            }
    }
    
    // Now I add phi to the objective function
    obj.addTerm(1, phi);
        
    // I tell cpllex to minimize the objective function
    model.addMinimize(obj);
    
    
    /*************** CONSTRAINTS IN THE MASTER PROBLEM ***************/
 
    /*
    * I create the constraints in the Master Problem. I do so by creating a
    * linear equation, IloLinearNumExpr, for each constraint end populating it. 
    * As mentioned in the top, I include constraint 1.b, 1.c, and 1.d in the 
    * Master Problem.
    */
    
    // Constraint 1b
    for(int g = 1; g <= ucp.getgPowerGenerators(); g++){
        for(int t = 1; t <= ucp.gettTimeperiods(); t++){
            IloLinearNumExpr rhs1b = model.linearNumExpr();
                rhs1b.addTerm(ucp.getStartupCost()[g-1], u[g-1][t-1]);
                 // Same way of handeling u(g, t-1) as in constraint 1b in 
                 // assignment2Model.
                if(t > 1+1e-9){
                rhs1b.addTerm(-ucp.getStartupCost()[g-1], u[g-1][t-2]);
                }else{}
            model.addGe (c[g-1][t-1], rhs1b);
        }
    }
    
    //Constraint 1c
    for(int g = 1; g<= ucp.getgPowerGenerators();  g++){
        for(int t = 1; t <= ucp.gettTimeperiods(); t++){
            /* 
            * In constraint 1c I have to sum from t' to T_(g,t)^(U). 
            * T_(g,t)^(U) is the minimum on-time in period  t. I use the 
            * minimum function in jave with the Math.min statement to create 
            * the expression for T_(g,t)^(U). Since the constraint has to hold 
            * for all g and t I make a third loop inside and let k run from t 
            * to T_(g,t)^(U) which I define as T. 
            */
           double T = Math.min(t + ucp.getMinUpTime()[g-1] -1, ucp.gettTimeperiods()) ; 
            IloLinearNumExpr lhs1c = model.linearNumExpr();    
           for(int k = t; k <= T; k++){ 
                 lhs1c.addTerm(1,u[g-1][k-1]);
                 lhs1c.addTerm(-1,u[g-1][t-1]);
                 // Same way of handeling u(g, t-1) as in constraint 1b in 
                 // assignment2Model.
                 if(t > 1+1e-9){
                 lhs1c.addTerm(1,u[g-1][t-2]);
                 }else{}
            }
            model.addGe(lhs1c, 0);        
        }
    }
    
    // Constraint 1d
        for(int g = 1; g<= ucp.getgPowerGenerators();  g++){
        for(int t = 1; t <= ucp.gettTimeperiods(); t++){
            /*
            * I handle T_(g,t)^(D) similarly as T_(g,t)^(U), just with the 
            * minimum downtime instead of minimum ontime. 
            */
           double T = Math.min(t + ucp.getMinDownTime()[g-1] -1, ucp.gettTimeperiods()) ; 
           IloLinearNumExpr lhs1d = model.linearNumExpr();     
           for(int k = t; k <= T; k++){
                 lhs1d.addTerm(-1,u[g-1][k-1]);
                 lhs1d.addTerm(1,u[g-1][t-1]);
                 // Same way of handeling u(g, t-1) as in constraint 1b
                 if(t > 1+1e-9){
                 lhs1d.addTerm(-1,u[g-1][t-2]);
                 }else{}
            }
            model.addGe(lhs1d, -T+t-1);
        }
    }
    
    }
        
    // I tell Cplex to solve the Master Problem.
        public void solve() throws IloException{    
        /*
        * I want Cplex to use the the class I name callback when solving the
        * problem. This callback class is created below.
        *
        * The callback class extends the LazyConstraintCallback which allows 
        * us to add constraints to the problem while it is being solved with
        * the branch and bound method. We want to use this feature, because it 
        * is a vital part of Benders Decomposition to add optimality and  
        * feasibility cuts when reaching an either non feasible or non optimal
        * solution.
        */
        
        model.use(new Callback());
        
        // Solve the problem
        model.solve();
    }
    
        
    // Now I create the class callback. 
    private class Callback extends IloCplex.LazyConstraintCallback{
 
        public Callback() {
        }

        /*
        * Since the callback class extendst LazyConstraintCallback, it needs
        * a main method. In this main method I use the features from the 
        * LazyConstraintCallback class to tell cplex how to check the 
        * feasibility and optimality of a solution and how to create optimality
        * and feasibility cuts if necessary. 
        *
        * @throws IloException 
        */

        // I use @Override since i am changing the solve method. 
        @Override
        protected void main() throws IloException {
            
            // First I obtain the solution at the current node so I can check 
            // wheather it is feasible and/or optimal. 
            double[][] U = getU();
            double Phi = getPhi();
            
            /* The next step in Benders decomposition for integer problems 
            * after obtaining an initial solution is to check wheather the 
            * solution is feasible or not. I do so by solving the feasibility
            * subproblem. This is created in a seperate file called 
            * FeasibilitySubProblem. Since c is not appearing in the subproblem
            * we only consider the solution u and phi.
            */

            FeasibilitySubProblem fsp = new FeasibilitySubProblem(ucp, U);
            fsp.solve();
            double fspObjective = fsp.getObjective();
        
            /*
            * After we solved the the subproble, we check wheather it is 
            * feasible. The subproblem is feasible if the objective value is 0.
            * I print out the objective value of the feasibility subproblem, so 
            * I can check wheather it is sensible if anything goes wrong.
            */

            System.out.println("FSP "+fspObjective);
           
            /*
            * If the objective value of the feasibility problem is positive
            * the feasibility subproblem is not feasible and a feasibility 
            * cut is needed. If I need a cut I get the linear and the constant 
            * terms for feasibility cut from the feasibility subproblem.
            * The cut will be LinearTerm <= -constantTerm.
            * The cut is added to the problem by using the method add() 
            * featured in the LazyConstraintCallback class.
            */
                
            if(fspObjective >= 0+1e-9){
                System.out.println("Generating feasibility cut");
                double constant = fsp.getCutConstant();
                IloLinearNumExpr linearTerm = fsp.getCutLinearTerm(u);           
                add(model.le(linearTerm, -constant));
                
                   }else{
                
            /*
            * If the objective value of the feasibility subproblem is not 
            * positive, the subproblem is feasible. This means that we
            * check if it is an optimal solution or if we have to add
            * an optimality cut to the problem. First I tell cplex to solve the 
            * optimality subproblem. 
            */
            
                OptimalitySubProblem osp = new OptimalitySubProblem(ucp,U);
                osp.solve();
                double ospObjective = osp.getObjective();
                
            /*
            * I print out the value of phi and the objective value of the 
            * optimality subproblem. This is done so I can verify the values 
            * if somthing goes wrong in the algorithm. 
            * The solution is optimal if phi >= objectiveValue of the Optimality 
            * subproblem. If this is the case I print out that it is optimal.
            */
                System.out.println("Phi "+Phi+ " OSP "+ospObjective );
                if(Phi >= ospObjective - 1e-9){
                    System.out.println("The current node is optimal");
                    
                }else{
            
            /*
            * If the solution is not optimal, I need to generate an optimality
            * cut. This is done by obtaining the linear and constant term from
            * the optimality subproblem. In this case I also print out that 
            * there is generated an optimality cut. As in the case with the 
            * fesibility cut I add the optimality cut with the method add().
            */
                     
                    System.out.println("Generating optimality cut");
                    double cutConstant = osp.getCutConstant();
                    IloLinearNumExpr cutTerm = osp.getCutLinearTerm(u);
                    cutTerm.addTerm(-1, phi);
                    add(model.le(cutTerm, -cutConstant));
                }
            }
        }
        
        /*
        * I want the U and the phi value at the current node, because I need 
        * it in Benders Decomposition. So I create a method that returns the 
        * the values. I create U and then populate each 
        * position (g,t) with the corresponding U value. I use the method
        * getvalue() which is a method from the LazyConstraintsCallback class. 
        * getvalue() does, in opposite to the nomal model.getvalue(), return the 
        * values of U and phi at the current node in the branch and bound and
        * not the optimal value after the program has ended. 
        *
        * @throws IloException 
        */
        public double[][] getU() throws IloException{
           double U[][] = new double[ucp.getgPowerGenerators()][ucp.gettTimeperiods()];
            for(int g = 1;g <= ucp.getgPowerGenerators(); g++){
                for(int t = 1; t <= ucp.gettTimeperiods(); t++){ 
               U[g-1][t-1] = getValue(u[g-1][t-1]);
           }}
           return U;
       }
       /**
        * @throws IloException 
        */
        public double getPhi() throws IloException{
           return getValue(phi);
        }
    }
    
    /*
     * I create a method that returns the objective value. 
     * @throws IloException 
     */
    public double getObjective() throws IloException{
        return model.getObjValue();
    }
    /*
    * Then I print the solution of the Benders Decomposition. 
    * I print out the optimal objective value and the solution to the u's. 
    * @throws IloException 
    */
    public void printSolution() throws IloException{
         for(int g = 1;g <= ucp.getgPowerGenerators(); g++){
                for(int t = 1; t <= ucp.gettTimeperiods(); t++){ 
            System.out.println( "Generator "+ucp.getGeneratorNames()[g-1]+ " in hour " +t+ " = "+model.getValue(u[g-1][t-1]));
            }
        }   
    }
    
    // I release the resources the IloCplex object uses.
    public void end(){
        model.end();
    }
    
}


