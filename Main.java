package assignment2;

import ilog.concert.IloException;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * @author Emilie Popp
 * 
 * I create the Main, which contains the Main method for the problem.  
 */
public class Main {

    /**
     * @param args 
     * @throws ilog.concert.IloException
     * @throws java.io.FileNotFoundException
     */
    
    public static void main(String[] args) throws IloException, FileNotFoundException{
       
        // I create the parameters in the Unit Commitment Problem.
        
        // We have 24 timeperiods and 31 powergenerators
        int tTimeperiods = 24;
        int gPowerGenerators = 31;

        double powerLB[] = new double[gPowerGenerators];
        double powerUB[] = new double[gPowerGenerators];       
        double startupCost[] = new double[gPowerGenerators];
        double commitmentCost[] = new double[gPowerGenerators];
        double rampUp[] = new double[gPowerGenerators];
        double rampDown[] = new double[gPowerGenerators];
        double minUpTime[] = new double[gPowerGenerators];
        double minDownTime[] = new double[gPowerGenerators];
        double productionCost[] = new double[gPowerGenerators];
        String generatorNames[] = new String[gPowerGenerators];
        
        // I import the data for the parameters from the txt-file "generators".
        // I replaced the dots in the file generators with kommas, otherwise my
        // computer would not read it, beacause it has danish settings.   
        File file = new File("C:\\Users\\Emilie\\Documents\\MatØk\\MIOR\\Assignment2\\generators.txt"); 
        
        // I create a scanner object so I can scan the file in order to choose 
        // the right data for my parameters.
        Scanner s = new Scanner(file);
    
        // I skip the first two lines in the generators file, since they only
        // contain the names of the coloums. 
        s.nextLine();
        s.nextLine();
        for(int i = 1 ; i <= gPowerGenerators ; i++){
            // The method .next() returns the next element as a string 
            String names = s.next();
            generatorNames[i-1] = names;
            // The method .nextdouble() returns the next element if it can be 
            // read as a double. Since it is only the first coloumn in the file 
            // that is not numbers I use .nextdouble() to return the remaining
            // values.
            double d1 = s.nextDouble();
            powerLB[i-1] = d1; 
            double d2 = s.nextDouble();
            powerUB[i-1] = d2; 
            double d3 = s.nextDouble();
            startupCost[i-1] = d3; 
            double d4 = s.nextDouble();
            commitmentCost[i-1] = d4; 
            double d5 = s.nextDouble();
            rampUp[i-1] = d5; 
            rampDown[i-1] = d5; 
            double d6 = s.nextDouble();
            minUpTime[i-1] = d6; 
            double d7 = s.nextDouble();
            minDownTime[i-1] = d7; 
            double d8 = s.nextDouble();
            productionCost[i-1] = d8; 
            
            // I print out the data to make sure it is correctly imported from
            // the generators.txt file. 
            System.out.println("Name = "+names+" d1 ="+d1+" d2= "+d2+ " d3= "+d3+ " d4= "+d4+ " d5= "+d5+ " d6= "+d6+ " d7= "+d7+ " d8= "+d8);}
        
            // The data for the power demand in the 24 hours are assigned to 
            // the double powerDemand.
        double powerDemand[] = new double[tTimeperiods];
            powerDemand[0] = 1344;
            powerDemand[1] = 1379;
            powerDemand[2] = 1425;
            powerDemand[3] = 1468;
            powerDemand[4] = 2010;
            powerDemand[5] = 2145;
            powerDemand[6] = 2334;
            powerDemand[7] = 2545;
            powerDemand[8] = 2230;
            powerDemand[9] = 2134;
            powerDemand[10] = 2200;
            powerDemand[11] = 2221;
            powerDemand[12] = 2345;
            powerDemand[13] = 2268;
            powerDemand[14] = 2100;
            powerDemand[15] = 1980;
            powerDemand[16] = 1992;
            powerDemand[17] = 2245;
            powerDemand[18] = 2345;
            powerDemand[19] = 2432;
            powerDemand[20] = 2564;
            powerDemand[21] = 2080;
            powerDemand[22] = 1800;
            powerDemand[23] = 1325;
        
            // Loadshedding cost is 46 in all hours 
        double loadShedding[] = new double [tTimeperiods];
        for(int t = 1; t<= tTimeperiods ; t++){ 
        loadShedding[t-1] = 46;}

        // I create an instance of the Unit Commitment Problem in assignment2
        assignment2Problem ucp = new assignment2Problem(tTimeperiods, gPowerGenerators, minUpTime, minDownTime, rampUp, rampDown, 
                commitmentCost, productionCost, startupCost, powerLB, powerUB, powerDemand, loadShedding, generatorNames);     
         
        //The Master Problem is created and solved. 
        masterProblem mp = new masterProblem(ucp);
        mp.solve();
        
        // I want to see the Optimal objective value when I solve the Unit 
        // Commitment Problem with Benders Decomposition.
        System.out.println("Optimal Benders objective value = "+mp.getObjective());
        mp.printSolution();
       
        // In order to check the validity of the results from the decomposition 
        // I solve the full model as it is written in the problem formulation.
        assignment2Model m = new assignment2Model(ucp);
        m.solve();
    
        // I compare the objective values from the two different methods. 
        System.out.println("Optimal Benders objective value = "+mp.getObjective());
        System.out.println("Optimal full model objective value = "+m.getObjectiveValue());
    }
}  
