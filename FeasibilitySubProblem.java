package assignment2;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;

/**
 * @author Emilie Popp
 * 
 * The Feasibility Subproblem is containing the constraints not included in 
 * the Master Problem and additional slack variable. The objective function 
 * in the Feasibiliity Subproblem is the sum of all the slackvariables. 
 * 
 */

public class FeasibilitySubProblem {
    private final IloCplex model; 
    private final IloNumVar p[][];
    private final IloNumVar l[];
    private final IloNumVar s1[];
    private final IloNumVar s2[][];
    private final IloNumVar s3[][];
    private final IloNumVar s4[][];
    private final IloNumVar s5[][];
    private final assignment2Problem ucp;
    private final IloRange powerBalanceConstraint[];
    private final IloRange minCapacityConstraint[][];
    private final IloRange maxCapacityConstraint[][];
    private final IloRange rampUpConstraint[][];
    private final IloRange rampDownConstraint[][];
   
    /*
     * Now I create the model for the feasibility subproblem.  
     *
     * @throws IloException 
     */
    
    public FeasibilitySubProblem(assignment2Problem ucp, double U[][]) throws IloException {
        this.ucp = ucp;
        
    // I create an IloCplex object, this is the object that will contain 
    // the the model.
        this.model = new IloCplex();
        
    /* 
    * The two decision variables in the feasibility subproblem is p and l
    * I create these as IloNumVar because they are continuous varibles. 
    * Since they are non-negative, I define them from 0 to +infinity. 
    */
    
    p = new IloNumVar[ucp.getgPowerGenerators()][ucp.gettTimeperiods()];
    l = new IloNumVar[ucp.gettTimeperiods()];

    for(int g = 1; g <= ucp.getgPowerGenerators(); g++){
        for(int t = 1; t <= ucp.gettTimeperiods(); t++){
                p[g-1][t-1] = model.numVar(0, Double.POSITIVE_INFINITY);
            }
        }    
    
    for(int t = 1; t <= ucp.gettTimeperiods(); t++){
                l[t-1] = model.numVar(0, Double.POSITIVE_INFINITY);
            }
    
    /*
    * Then I create the slack variables in the Feasibility Subproblem. They 
    * are also continouos variables defined from 0 to +infinity, so they are 
    * created similarly to the decision variables. 
    */
    this.s1 = new IloNumVar[ucp.gettTimeperiods()];
       for(int t = 1; t <= ucp.gettTimeperiods(); t++){
            s1[t-1] = model.numVar(0, Double.POSITIVE_INFINITY);
        }
    this.s2 = new IloNumVar[ucp.getgPowerGenerators()][ucp.gettTimeperiods()];
        for(int g = 1; g <= ucp.getgPowerGenerators(); g++){
            for(int t = 1; t <= ucp.gettTimeperiods(); t++){
            s2[g-1][t-1] = model.numVar(0, Double.POSITIVE_INFINITY);
        }
       }
    this.s3 = new IloNumVar[ucp.getgPowerGenerators()][ucp.gettTimeperiods()];
        for(int g = 1; g <= ucp.getgPowerGenerators(); g++){
            for(int t = 1; t <= ucp.gettTimeperiods(); t++){
            s3[g-1][t-1] = model.numVar(0, Double.POSITIVE_INFINITY);
        }
       }
    this.s4 = new IloNumVar[ucp.getgPowerGenerators()][ucp.gettTimeperiods()];
        for(int g = 1; g <= ucp.getgPowerGenerators(); g++){
            for(int t = 1; t <= ucp.gettTimeperiods(); t++){
            s4[g-1][t-1] = model.numVar(0, Double.POSITIVE_INFINITY);
        }
       }
    this.s5 = new IloNumVar[ucp.getgPowerGenerators()][ucp.gettTimeperiods()];
        for(int g = 1; g <= ucp.getgPowerGenerators(); g++){
            for(int t = 1; t <= ucp.gettTimeperiods(); t++){
            s5[g-1][t-1] = model.numVar(0, Double.POSITIVE_INFINITY);
        }
       }

    /*************** OBJECTIVE FUNCTION ***************/
    
    // The objective function for the feasibility subproblem is created.
    // I create an empty linear equation that I can add terms to.
    
    IloLinearNumExpr obj = model.linearNumExpr();
    
    // Then I add terms to the objetive function. The objective function 
    // consist of the sum of all the slack variables. 
    
        for(int t = 1; t >= ucp.gettTimeperiods(); t++){
                obj.addTerm(1, s1[t-1]);
        }
        for(int g = 1; g <= ucp.getgPowerGenerators() ; g++){
            for(int t = 1; t<= ucp.gettTimeperiods(); t++){
                obj.addTerm(1, s2[g-1][t-1]);
                obj.addTerm(1, s3[g-1][t-1]);
                obj.addTerm(1, s4[g-1][t-1]);
                obj.addTerm(1, s5[g-1][t-1]);
            }
        }
    
    //I tell cplex to minimize the objective function
    model.addMinimize(obj);
    
    
    /****************** CONSTRAINTS ******************/
    
    /*
    * Now I create the constraints in the feasibility subproblem. 
    * I do so by creating a linear equation, IloLinearNumExpr, for each 
    * constraint end populating it. The constraints in the feasibility sub-
    * problem is the constraints not included in the Master Problem, but with 
    * the slack variables. 
    */
    
    /* Constraint 1e
    * Since l(t) works like a positive slack variable, I only need to add the
    * negative slack variable. Thats the reason I only have one slack variable
    * in the constraint that should hold with equality. 
    */
    this.powerBalanceConstraint = new IloRange[ucp.gettTimeperiods()]; 
    for(int t= 1; t <= ucp.gettTimeperiods(); t++){
        IloLinearNumExpr lhs = model.linearNumExpr();
            for(int g = 1; g <= ucp.getgPowerGenerators(); g++){    
                lhs.addTerm(1, p[g-1][t-1]);
            }
            lhs.addTerm(1, l[t-1]);
            lhs.addTerm(-1, s1[t-1]);
      powerBalanceConstraint[t-1] = model.addEq(lhs, ucp.getPowerDemand()[t-1]);
    }

    // Constraint 1f
    this.minCapacityConstraint = new IloRange[ucp.getgPowerGenerators()][ucp.gettTimeperiods()];
    for(int g = 1;g <= ucp.getgPowerGenerators(); g++){
        for(int t = 1; t <= ucp.gettTimeperiods(); t++){     
           IloLinearNumExpr lhs1f = model.linearNumExpr();  
           lhs1f.addTerm(1, p[g-1][t-1]); 
           lhs1f.addTerm(1, s2[g-1][t-1]);
           // Notice that I use U(g,t) and not u(g,t), where U(g,t) is the 
           // solution at the current node in the branch and bound. 
           // I also use U(g,t) in the following constraints. 
        minCapacityConstraint[g-1][t-1] = model.addGe(lhs1f, ucp.getPowerLB()[g-1]*U[g-1][t-1]);
        }
    } 

    // Constraint 1g
    this.maxCapacityConstraint = new IloRange[ucp.getgPowerGenerators()][ucp.gettTimeperiods()];
    for(int g = 1; g <= ucp.getgPowerGenerators(); g++){
        for(int t = 1; t <= ucp.gettTimeperiods(); t++){    
           IloLinearNumExpr lhs1g = model.linearNumExpr();  
           lhs1g.addTerm(1, p[g-1][t-1]); 
           lhs1g.addTerm(-1, s3[g-1][t-1]);
        maxCapacityConstraint[g-1][t-1] = model.addLe(lhs1g, ucp.getPowerUB()[g-1]*U[g-1][t-1]);
        }
    } 
    
    // Constraint 1h
    this.rampUpConstraint = new IloRange[ucp.getgPowerGenerators()][ucp.gettTimeperiods()];
    for(int g = 1; g <= ucp.getgPowerGenerators(); g++){
        for(int t = 1; t <= ucp.gettTimeperiods(); t++){    
           IloLinearNumExpr lhs1h = model.linearNumExpr();  
           lhs1h.addTerm(1, p[g-1][t-1]);
           /*
           * Since we assume that all power generators are switched off at 
           * time 0, the poweroutput at time 0, p(g,0), must be 0. 
           * we have to subtract p(g, t-1), where t runs from 1 to 24. 
           * To handle this I make an If-statement, 
           * where I say that if t is bigger than 1, then I will subtract
           * the startupcost times p(g, t-1), since java start indexing 
           * from 0 it will be p(g-1, t-2). And else (if t=1) I will 
           * subtract 0*(the startup cost)=0 and therefore add nothing.
           */
           if(t > 1+1e-9){
           lhs1h.addTerm(-1, p[g-1][t-2]);
           }else{}
           lhs1h.addTerm(-1, s4[g-1][t-1]);
        rampUpConstraint[g-1][t-1] = model.addLe(lhs1h, ucp.getRampUp()[g-1]);
        } 
    }
    // Constraint 1i
    this.rampDownConstraint = new IloRange[ucp.getgPowerGenerators()][ucp.gettTimeperiods()];
    for(int g = 1;g <= ucp.getgPowerGenerators(); g++){
        for(int t = 1; t <= ucp.gettTimeperiods(); t++){    
           IloLinearNumExpr lhs1i = model.linearNumExpr();  
           lhs1i.addTerm(-1, p[g-1][t-1]);
        // Same way of handeling p(g, t-1) as in constraint 1h
           if(t > 1+1e-9){
           lhs1i.addTerm(1, p[g-1][t-2]);
           }else{}
           lhs1i.addTerm(-1, s5[g-1][t-1]);
        rampDownConstraint[g-1][t-1] = model.addLe(lhs1i, ucp.getRampDown()[g-1]);
        } 
    }  
}

    /*
     * I tell Cplex to solve the feasibility subproblem. 
     * @throws IloException 
     */
    public void solve() throws IloException{
        model.setOut(null);
        model.solve();
    }

    /*
     * I create a method that returns the objective value of the feasibility 
     * subproblem. 
     * @throws IloException 
     */
    public double getObjective() throws IloException{
        return model.getObjValue();
    }

    /*
    * I want to create the feasibility cut in case the solution from the 
    * current node in the branch and bound is not fesible. 
    * For this I need the dual variables for the different constraints. These 
    * are obtained by model.getDual() where I choose which constraint I want
    * the dual from. Then multiply the dual by the righthandside of the constraint 
    * and then sum over all g and t and set smaller than or equal to 0 to 
    * create the cut. In this case I create a constant part of the cut and 
    * a linear part of the cut. ´
    *
    * The first part of the feasibility cut is constant (does not depend on u). 
    *
    * @throws IloException 
    */
    public double getCutConstant() throws IloException{
        double constant = 0;
        for(int t = 1; t <= ucp.gettTimeperiods() ; t++){
            constant = constant + model.getDual(powerBalanceConstraint[t-1]) * ucp.getPowerDemand()[t-1];
        for(int g = 1; g <= ucp.getgPowerGenerators() ; g++){
            constant = constant + model.getDual(rampUpConstraint[g-1][t-1])*ucp.getRampUp()[g-1] +
                    model.getDual(rampDownConstraint[g-1][t-1])* ucp.getRampDown()[g-1];
        }
    } 
        return constant;
    }
    /*
    * The second part of the feasibility cut is linear in u. It is created as 
    * an empty linear expression that I add terms to. 
    * 
    * @throws IloException 
    */
    public IloLinearNumExpr getCutLinearTerm(IloIntVar u[][]) throws IloException{
        IloLinearNumExpr cutTerm = model.linearNumExpr();
        for(int g = 1;g <= ucp.getgPowerGenerators(); g++){
            for(int t = 1; t <= ucp.gettTimeperiods(); t++){ 
                cutTerm.addTerm(model.getDual(minCapacityConstraint[g-1][t-1])* ucp.getPowerLB()[g-1], u[g-1][t-1]);
                cutTerm.addTerm(model.getDual(maxCapacityConstraint[g-1][t-1])* ucp.getPowerUB()[g-1], u[g-1][t-1]);
            }
        }
        return cutTerm;
    }
    
    // I release the resources the IloCplex object uses.
    public void end(){
        model.end();
    }
    
}
