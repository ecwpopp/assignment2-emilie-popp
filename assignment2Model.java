package assignment2;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;

/**
 *
 * @author Emilie Popp
 * 
 * In order to check wheather my Benders Decomposition algorithm is finding 
 * the optimal solution and optimal objective value, I create a class where I
 * describe the model of the original Unit Commitment Problem and tell 
 * Cplex to solve it. 
 */

public class assignment2Model {
     
    private final IloCplex model;
    private final IloIntVar u[][];
    private final IloNumVar c[][];
    private final IloNumVar p[][];
    private final IloNumVar l[];
    private final assignment2Problem ucp;
    
    /*
    * I start creating a model for the original problem 
    * without decomposition, just as in the problem formulation.
    */
    public assignment2Model(assignment2Problem ucp) throws IloException{
    this.ucp = ucp;
    
    // I create an IloCplex object, this object will contain 
    // the description of the model.
    this.model =new IloCplex();
    
    /* 
    * Now I create the decision variables in the problem
    * For all the variables I assign an IloNumVar or IloIntVar object to 
    * each position in order to tell cplex that it is a decision variable.
    *
    * The first variable is the complicating binary variable u(g,t) 
    * deciding wheather generator g is on/off at time t.
    * I tell cplex that u is integer and binary by using IloIntVar and 
    * tell that each position u[g-1][t-1] is a boolean variable.
    */
    u = new IloIntVar[ucp.getgPowerGenerators()][ucp.gettTimeperiods()];
   
    for(int g = 1; g <= ucp.getgPowerGenerators(); g++){
        for(int t = 1; t <= ucp.gettTimeperiods(); t++){
                u[g-1][t-1] = model.boolVar() ;
            }
        } 
    
    /* The other decision variables are continues variables. Therefore I create 
    * IloNumVar objects and since thay are positive I let them be in the 
    * interval from 0 to infinity. There are three continues variables c(g,t) is 
    * the startup cost when switching on generator g in period t, p(g,t) is the 
    * power output of generator g in period t and l(t) is the load-shedding 
    * in period t to make sure there is balance between demand and production. 
    */
    c = new IloNumVar[ucp.getgPowerGenerators()][ucp.gettTimeperiods()];
    p = new IloNumVar[ucp.getgPowerGenerators()][ucp.gettTimeperiods()];
    l = new IloNumVar[ucp.gettTimeperiods()];
    
    for(int g = 1; g<= ucp.getgPowerGenerators(); g++){
        for(int t = 1; t <= ucp.gettTimeperiods(); t++){
                c[g-1][t-1] = model.numVar(0, Double.POSITIVE_INFINITY);
                p[g-1][t-1] = model.numVar(0, Double.POSITIVE_INFINITY);
            }
        }  
    
    for(int t = 1; t<= ucp.gettTimeperiods(); t++){
                l[t-1] = model.numVar(0, Double.POSITIVE_INFINITY);
            }
    
    /*************** OBJECTIVE FUNCTION ***************/
    
    // The objective function for the problem is created by an empty linear
    // equation I can add terms to.
        IloLinearNumExpr obj = model.linearNumExpr();
        
    // Then I add the terms, the first double sum is added. 
    for(int g = 1; g<= ucp.getgPowerGenerators(); g++){
            for(int t = 1; t <= ucp.gettTimeperiods() ; t++){
                obj.addTerm(1, c[g-1][t-1]);
                obj.addTerm(ucp.getCommitmentCost()[g-1], u[g-1][t-1]);                
            }
        }
    
    // The sum of the load-shedding is added.
    for(int t = 1; t<= ucp.gettTimeperiods(); t++){
            obj.addTerm(ucp.getLoadShedding()[t-1], l[t-1]);
        }
    
    // Then the sum of the production cost times the poweroutput is added.
    for(int g = 1; g<= ucp.getgPowerGenerators(); g++){
            for(int t = 1; t <= ucp.gettTimeperiods() ; t++){
                obj.addTerm(ucp.getProductionCost()[g-1], p[g-1][t-1]);
            }
        }
    
    // I tell cpllex to minimize the objective function.
    model.addMinimize(obj);
    
    /*************** CONSTRAINTS ***************/
    
    /*
    * Now I create the constraints in the problem. I do so by creating a
    * linear equation, IloLinearNumExpr, for each constraint and populating it. 
    */
    
    // Constraint 1b
    for(int g = 1; g <= ucp.getgPowerGenerators(); g++){
        for(int t = 1; t <= ucp.gettTimeperiods(); t++){
            IloLinearNumExpr rhs1b = model.linearNumExpr();
                rhs1b.addTerm(ucp.getStartupCost()[g-1], u[g-1][t-1]);
                /* 
                * In the constraint 1b we have to subtract u(g, t-1), where 
                * t runs from 1 to 24. For t=1 we have u(g,0)=0 given in the 
                * problem formulation. To handle this I make an If-statement, 
                * where I say that if t is bigger than 1, then I will subtract
                * the startupcost times u(g, t-1), since java start indexing 
                * from 0 it will be u(g-1, t-2). And else (if t=1) I will 
                * subtract 0*(the startup cost)=0 and therefore add nothing.
                * I have done the exact same thing in the other constraints 
                * where I have to deal with u(g, t-1).
                */
                if(t > 1+1e-9){
                rhs1b.addTerm(-ucp.getStartupCost()[g-1], u[g-1][t-2]);
                }else{}
            model.addGe(c[g-1][t-1], rhs1b);
        }
    }
    
    //Constraint 1c
    for(int g = 1; g <= ucp.getgPowerGenerators(); g++){
        for(int t = 1; t <= ucp.gettTimeperiods(); t++){
           /* 
            * In constraint 1c I have to sum from t'=t to T_(g,t)^(U). 
            * T_(g,t)^(U) is the minimum on-time in period  t. I use the 
            * minimum function in jave with the Math.min statement to create 
            * the expression for T_(g,t)^(U). Since the constraint has to hold 
            * for all g and t I make a third loop inside and let k run from t 
            * to T_(g,t)^(U) which I define as T. 
            */
           double T = Math.min(t + ucp.getMinUpTime()[g-1] -1, ucp.gettTimeperiods()) ; 
            IloLinearNumExpr lhs1c = model.linearNumExpr();     
           for(int k = t; k <= T; k++){
                 lhs1c.addTerm(1,u[g-1][k-1]);
                 lhs1c.addTerm(-1,u[g-1][t-1]);
                 // Same way of handeling u(g, t-1) as in constraint 1b
                 if(t > 1+1e-9){
                 lhs1c.addTerm(1,u[g-1][t-2]);
                 }else{}
                }
            model.addGe(lhs1c, 0);
        }
    }
    
    // Constraint 1d
        for(int g = 1; g<= ucp.getgPowerGenerators();  g++){
        for(int t = 1; t <= ucp.gettTimeperiods(); t++){
            /*
            * I handle T_(g,t)^(D) similarly as T_(g,t)^(U), just with the 
            * minimum downtime instead of minimum ontime. 
            */
           double T = Math.min(t + ucp.getMinDownTime()[g-1] -1, ucp.gettTimeperiods()) ; 
           IloLinearNumExpr lhs1d = model.linearNumExpr();  
           for(int k = t; k <= T; k++){   
                 lhs1d.addTerm(-1,u[g-1][k-1]);
                 lhs1d.addTerm(1,u[g-1][t-1]);
                 // Same way of handeling u(g, t-1) as in constraint 1b
                 if(t > 1+1e-9){
                 lhs1d.addTerm(-1,u[g-1][t-2]);
                 }else{}
                }
            //I put the sum from t'=t to T_(g,t)^(D) of 1 to the right hand side.
            model.addGe(lhs1d, -T+t-1);
        }
    }
    
    // Constraint 1e
    for(int t = 1; t <= ucp.gettTimeperiods(); t++){
        IloLinearNumExpr lhs1e = model.linearNumExpr();
           for(int g = 1; g<= ucp.getgPowerGenerators();  g++){
            lhs1e.addTerm(1, p[g-1][t-1]); }
            lhs1e.addTerm(1, l[t-1]);
            model.addEq(lhs1e, ucp.getPowerDemand()[t-1]);
        }
    
    // Constraint 1f
    for(int g = 1;g <= ucp.getgPowerGenerators(); g++){
        for(int t = 1; t <= ucp.gettTimeperiods(); t++){    
           IloLinearNumExpr rhs1f = model.linearNumExpr();  
           rhs1f.addTerm(ucp.getPowerLB()[g-1], u[g-1][t-1]);
           model.addGe(p[g-1][t-1], rhs1f);
        }
    } 

    // Constraint 1g
    for(int g = 1;g <= ucp.getgPowerGenerators(); g++){
        for(int t = 1; t <= ucp.gettTimeperiods(); t++){    
           IloLinearNumExpr rhs1g = model.linearNumExpr();  
           rhs1g.addTerm(ucp.getPowerUB()[g-1], u[g-1][t-1]);
           model.addLe(p[g-1][t-1], rhs1g);
        }
    } 
    
    // Constraint 1h
    for(int g = 1; g <= ucp.getgPowerGenerators(); g++){
        for(int t = 1; t <= ucp.gettTimeperiods(); t++){    
           IloLinearNumExpr lhs1h = model.linearNumExpr();  
           lhs1h.addTerm(1, p[g-1][t-1]);
           /* 
           * Since we all power generators are switched off at 
           * time 0, the poweroutput at time 0, p(g,0), must be 0. 
           * I use the same way of handeling p(g, t-1) as I used to u(g, t-1)
           * in constraint 1b.
           */
           if(t > 1+1e-9){
           lhs1h.addTerm(-1, p[g-1][t-2]);
           }else{}
           model.addLe(lhs1h, ucp.getRampUp()[g-1]);
        } 
    }
    
    // Constraint 1i
    for(int g = 1;g <= ucp.getgPowerGenerators(); g++){
        for(int t = 1; t <= ucp.gettTimeperiods(); t++){    
           IloLinearNumExpr lhs1i = model.linearNumExpr();  
           lhs1i.addTerm(-1, p[g-1][t-1]);
           // Same way of handeling p(g, t-1) as in constraint 1h 
           if(t > 1+1e-9){
           lhs1i.addTerm(1, p[g-1][t-2]);
           }else{}
           model.addLe(lhs1i, ucp.getRampDown()[g-1]);
        } 
    }    
}    
    /* 
    * Now I tell cplex to solve the model and print the optimal 
    * objective value and the optimal solution of the u's in the problem.
    * @throws ilog.concert.IloException 
    */
    
    public void solve() throws IloException{
        model.setOut(null);
        model.solve();
        System.out.println("Optimal objective value "+model.getObjValue());
        System.out.println("Optimal solution ");
        for(int g = 1; g <= ucp.getgPowerGenerators(); g++){
            for(int t = 1; t <= ucp.gettTimeperiods(); t++ ){
            System.out.println( "Generator "+ucp.getGeneratorNames()[g-1]+ " in hour " +t+ " = "+model.getValue(u[g-1][t-1]));
            }
        }
    }
    
    // To be able to acces the objective value in the Main, I create 
    // a method getObjectiveValue, that returns the objective value of the model.
    
    public double getObjectiveValue() throws IloException{
    return model.getObjValue();
    }
    
    
    // I release the resources the IloCplex object uses.
    public void end(){
    model.end();}
}