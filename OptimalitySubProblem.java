package assignment2;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;

/**
 *
 * @author Emilie Popp
 * 
 * The optimality subproblem consist of the part of the original problem 
 * that is not included in the Master Problem. 
 */
public class OptimalitySubProblem {
    
    private final IloCplex model;
    private final IloNumVar l[];
    private final IloNumVar p[][];
    private final assignment2Problem ucp;
    private final IloRange powerBalanceConstraint[];
    private final IloRange minCapacityConstraint[][];
    private final IloRange maxCapacityConstraint[][];
    private final IloRange rampUpConstraint[][];
    private final IloRange rampDownConstraint[][];
    
    /*
     * I create the model for the Optimality Subproblem. 
     *
     * @throws IloException 
     */
    public OptimalitySubProblem(assignment2Problem ucp, double U[][]) throws IloException {
        this.ucp = ucp;
        
                
    // I create an IloCplex object, this is the object that will contain 
    // the the model.
        this.model = new IloCplex();
        
            
    /* 
    * The two decision variables in the optimality subproblem is p and l
    * I create these as IloNumVar because they are continuous varibles. 
    * Since they are non-negative, I define them from 0 to +infinity. 
    */
    p = new IloNumVar[ucp.getgPowerGenerators()][ucp.gettTimeperiods()];
    l = new IloNumVar[ucp.gettTimeperiods()];

    for(int g = 1; g<= ucp.getgPowerGenerators(); g++){
        for(int t = 1; t <= ucp.gettTimeperiods(); t++){
                p[g-1][t-1] = model.numVar(0, Double.POSITIVE_INFINITY);
            }
        }    
    
    for(int t = 1; t<= ucp.gettTimeperiods(); t++){
                l[t-1] = model.numVar(0, Double.POSITIVE_INFINITY);
            }
            
    /*************** OBJECTIVE FUNCTION ***************/
    
    // I now create the objective function for the optimality subproblem 
    // I create an empty linear equation  that I can add terms to.
    
        IloLinearNumExpr obj = model.linearNumExpr();

    // Then I add terms to the objetive function. The objective function 
    // consist of the terms in the objective function from the original 
    // problem, that are not included in the master problem. 
        
    //The sum over the load shedding 
    for(int t = 1; t<= ucp.gettTimeperiods(); t++){
                obj.addTerm(ucp.getLoadShedding()[t-1], l[t-1]);
            }
    
    //Them sum over the production cost for generator g times the poweroutput
    for(int g = 1; g<= ucp.getgPowerGenerators(); g++){
            for(int t = 1; t <= ucp.gettTimeperiods() ; t++){
                obj.addTerm(ucp.getProductionCost()[g-1], p[g-1][t-1]);
            }
        }
    
    // I tell cpllex to minimize the objective function
    model.addMinimize(obj);
    

    /****************** CONSTRAINTS ******************/
    
    /*
    * Now I create the constraints in the optimality subproblem. 
    * I do so by creating a linear equation, IloLinearNumExpr, for each 
    * constraint end populating it. The constraints in the optimality sub-
    * problem is the constraints not included in the Master Problem.
    */
       
    // Constraint 1e
    this.powerBalanceConstraint = new IloRange[ucp.gettTimeperiods()]; 
    for(int t = 1; t <= ucp.gettTimeperiods(); t++){
        IloLinearNumExpr lhs1e = model.linearNumExpr();
           for(int g = 1; g<= ucp.getgPowerGenerators();  g++){
            lhs1e.addTerm(1, p[g-1][t-1]); }
            lhs1e.addTerm(1, l[t-1]);
            powerBalanceConstraint[t-1] = model.addEq(lhs1e, ucp.getPowerDemand()[t-1]);
    }
    
    // Constraint 1f
    this.minCapacityConstraint = new IloRange[ucp.getgPowerGenerators()][ucp.gettTimeperiods()];
    for(int g = 1;g <= ucp.getgPowerGenerators(); g++){
        for(int t = 1; t <= ucp.gettTimeperiods(); t++){     
           IloLinearNumExpr lhs1f = model.linearNumExpr();  
           lhs1f.addTerm(1, p[g-1][t-1]); 
           // Notice that I use U(g,t) and not u(g,t), where U(g,t) is the 
           // solution at the current node in the branch and bound. 
           // I also use U(g,t) in the following constraints. 
        minCapacityConstraint[g-1][t-1] = model.addGe(lhs1f, ucp.getPowerLB()[g-1]*U[g-1][t-1]);
        }
    } 

    // Constraint 1g
    this.maxCapacityConstraint = new IloRange[ucp.getgPowerGenerators()][ucp.gettTimeperiods()];
    for(int g = 1;g <= ucp.getgPowerGenerators(); g++){
        for(int t = 1; t <= ucp.gettTimeperiods(); t++){    
           IloLinearNumExpr lhs1g = model.linearNumExpr();  
           lhs1g.addTerm(1, p[g-1][t-1]); 
        maxCapacityConstraint[g-1][t-1] = model.addLe(lhs1g, ucp.getPowerUB()[g-1]*U[g-1][t-1]);
        }
    } 
    
    // Constraint 1h
    this.rampUpConstraint = new IloRange[ucp.getgPowerGenerators()][ucp.gettTimeperiods()];
    for(int g = 1;g <= ucp.getgPowerGenerators(); g++){
        for(int t = 1; t <= ucp.gettTimeperiods(); t++){    
           IloLinearNumExpr lhs1h = model.linearNumExpr();  
           lhs1h.addTerm(1, p[g-1][t-1]);
           /*
           * Since we assume that all power generators are switched off at 
           * time 0, the poweroutput at time 0, p(g,0), must be 0. 
           * we have to subtract p(g, t-1), where t runs from 1 to 24. 
           * To handle this I make an If-statement, 
           * where I say that if t is bigger than 1, then I will subtract
           * the startupcost times p(g, t-1), since java start indexing 
           * from 0 it will be p(g-1, t-2). And else (if t=1) I will 
           * subtract 0*(the startup cost)=0 and therefore add nothing.
           */
           if(t > 1+1e-9){
           lhs1h.addTerm(-1, p[g-1][t-2]);
           }else{}
        rampUpConstraint[g-1][t-1] = model.addLe(lhs1h, ucp.getRampUp()[g-1]);
        } 
    }
    // Constraint 1i
    this.rampDownConstraint = new IloRange[ucp.getgPowerGenerators()][ucp.gettTimeperiods()];
    for(int g = 1;g <= ucp.getgPowerGenerators(); g++){
        for(int t = 1; t <= ucp.gettTimeperiods(); t++){    
           IloLinearNumExpr lhs1i = model.linearNumExpr();  
        // Same way of handeling p(g, t-1) as in constraint 1h
           lhs1i.addTerm(-1, p[g-1][t-1]);
           if(t > 1+1e-9){
           lhs1i.addTerm(1, p[g-1][t-2]);
           }else{}
        rampDownConstraint[g-1][t-1] = model.addLe(lhs1i, ucp.getRampDown()[g-1]);
        } 
    }        
}
    
    /*
    * I tell Cplex to solve the optimality subproblem. 
    * @throws IloException 
    */
    public void solve() throws IloException{
        model.setOut(null);
        model.solve();
    }

    
    /*
     * I create a method that returns the objective value of the optimality 
     * subproblem. 
     * @throws IloException 
     */
    public double getObjective() throws IloException{
        return model.getObjValue();
    }
    
    /*
    * I want to create the optimality cut in case the solution from the 
    * current node in the branch and bound is not optimal. 
    *
    * As in the Feasibility cut I obtain the dual values with model.getDual()
    * and then create the linear and the constant term of the optimalitycut.
    *
    * The first part of the optimality cut is constant (does not depend on u). 
    *
    * @throws IloException 
    */
    
    public double getCutConstant() throws IloException{
    double constant = 0;
        for(int t = 1; t <= ucp.gettTimeperiods() ; t++){
        constant = constant + model.getDual(powerBalanceConstraint[t-1]) * ucp.getPowerDemand()[t-1];
            for(int g = 1; g <= ucp.getgPowerGenerators() ; g++){
        constant = constant + model.getDual(rampUpConstraint[g-1][t-1])*ucp.getRampUp()[g-1] +
                model.getDual(rampDownConstraint[g-1][t-1])* ucp.getRampDown()[g-1];
        }
    } 
        return constant;
    }

    /*
    * The second part of the optimality cut is a linear in u. It is created as 
    * an empty linear expression that I add terms to. 
    * 
    * @throws IloException 
    */
    public IloLinearNumExpr getCutLinearTerm(IloIntVar u[][]) throws IloException{
        IloLinearNumExpr cutTerm = model.linearNumExpr();
        
    for(int g = 1;g <= ucp.getgPowerGenerators(); g++){
        for(int t = 1; t <= ucp.gettTimeperiods(); t++){ 
            cutTerm.addTerm(model.getDual(minCapacityConstraint[g-1][t-1])* ucp.getPowerLB()[g-1], u[g-1][t-1]);
            cutTerm.addTerm(model.getDual(maxCapacityConstraint[g-1][t-1])* ucp.getPowerUB()[g-1], u[g-1][t-1]);
            }
        }
        return cutTerm;
    }
    
    // I release the resources the IloCplex object uses.
    public void end(){
        model.end();
    }
}

    
