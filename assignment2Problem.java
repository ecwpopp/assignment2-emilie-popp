package assignment2;

/**
 * @author Emilie Popp
 *
 * I create a class that contains a template for the Unit Commitment Problem. 
 */


public class assignment2Problem {
    /*
    * Here I define the type of the parameter vectors in the Unit Commitment 
    * Problem. I use the statement final because I assign one value to the 
    * parameter vectors and I do not change it afterwards.  
    */
    
    private final int tTimeperiods;
    private final int gPowerGenerators;
    private final double minUpTime[];
    private final double minDownTime[];
    private final double rampUp[];
    private final double rampDown[];
    private final double commitmentCost[];
    private final double productionCost[];
    private final double startupCost[];
    private final double powerLB[];
    private final double powerUB[];
    private final double powerDemand[];
    private final double loadShedding[];
    private final String generatorNames[];
    
    // This is the constructor of the Unit Commitment Problem. 
    public assignment2Problem(int tTimeperiods, int gPowerGenerators, double[] minUpTime, double[] minDownTime, double[] rampUp, double[] rampDown, double[] commitmentCost,
        double[] productionCost, double[] startupCost, double[] powerLB, double[] powerUB, double[] powerDemand, double[] loadShedding, String generatorNames[]) {
        this.tTimeperiods = tTimeperiods;
        this.gPowerGenerators = gPowerGenerators;
        this.minUpTime = minUpTime;
        this.minDownTime = minDownTime;
        this.rampUp = rampUp;
        this.rampDown = rampDown;
        this.commitmentCost = commitmentCost;
        this.productionCost = productionCost;
        this.startupCost = startupCost;
        this.powerLB = powerLB;
        this.powerUB = powerUB;
        this.powerDemand = powerDemand;
        this.loadShedding = loadShedding;
        this.generatorNames = generatorNames;
    }

    // I create a method for each parameter so I can acces the data they 
    // contain. These getters returns the content of the parameters. 
    
    public int gettTimeperiods() {
        return tTimeperiods;
    }

    public int getgPowerGenerators() {
        return gPowerGenerators;
    }

    public double[] getMinUpTime() {
        return minUpTime;
    }

    public double[] getMinDownTime() {
        return minDownTime;
    }

    public double[] getRampUp() {
        return rampUp;
    }

    public double[] getRampDown() {
        return rampDown;
    }

    public double[] getCommitmentCost() {
        return commitmentCost;
    }

    public double[] getProductionCost() {
        return productionCost;
    }

    public double[] getStartupCost() {
        return startupCost;
    }

    public double[] getPowerLB() {
        return powerLB;
    }

    public double[] getPowerUB() {
        return powerUB;
    }

    public double[] getPowerDemand() {
        return powerDemand;
    }

    public double[] getLoadShedding() {
        return loadShedding;
    }

    public String[] getGeneratorNames() {
        return generatorNames;
    }
        
}
